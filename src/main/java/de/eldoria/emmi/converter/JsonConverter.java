package de.eldoria.emmi.converter;

import de.eldoria.emmi.converter.placeholder.Placeholder;
import de.eldoria.emmi.fragments.collections.EmmiFragmentCollection;
import de.eldoria.emmi.utilities.enums.Colors;

/**
 * JsonConverter object converts a EMMI string in to an JSON string.
 */
public final class JsonConverter {

    private JsonConverter() {
    }

    /**
     * Converts a EMMI message to a JSON string.
     *
     * @param message      EMMI message
     * @param placeHolders placeholder array
     * @return JSON string
     */
    public static String convert(String message, Placeholder... placeHolders) {
        String messageWithoutPlaceholder = applyPlaceholder(message, placeHolders);

        return new EmmiFragmentCollection(messageWithoutPlaceholder).toJsonFragmentCollection().toString();
    }

    /**
     * Converts a EMMI message to a JSON string. Changes the default color of the message
     *
     * @param message      EMMI message
     * @param defaultColor The default color as a Colors enum.
     * @param placeholders placeholder array
     * @return JSON sring
     */
    public static String convert(String message, Colors defaultColor, Placeholder... placeholders) {
        String newDefaultColor = "[c=" + Colors.getColorChar(defaultColor) + "]";
        String result = newDefaultColor + message.replace("[c=r]", newDefaultColor);

        return convert(result, placeholders);
    }

    /**
     * Applies the placeholder.
     *
     * @param message      The message where the placeholders should be applied
     * @param placeHolders One or more placeholders.
     * @return return a string with the placeholder replaced
     */
    private static String applyPlaceholder(String message, Placeholder... placeHolders) {
        String convertedMessage = message;

        for (Placeholder placeHolder : placeHolders) {
            convertedMessage = placeHolder.apply(convertedMessage);
        }

        return convertedMessage;
    }
}